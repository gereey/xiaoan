from scrapy import cmdline
import argparse
parser = argparse.ArgumentParser(description='这是一个示例程序，演示如何使用argparse读取命令行参数。')

# 添加命令行参数
parser.add_argument('--taskID', type=str, required=True)
parser.add_argument('--url', type=str, required=True)

# 解析命令行参数
args = parser.parse_args()

allowed_domains = args.url
start_urls = args.url
zf_domain = "zfai.buaa.edu.cn"
# https://news.buaa.edu.cn/
# zf_url = 'https://news.buaa.edu.cn/info/1002/63586.htm'
# test = 'https://zfai.buaa.edu.cn/info/1141/3113.htm'
command = 'scrapy crawl news -a url='+start_urls+' -a domain=' + 'news.buaa.edu.cn' + ' -a taskID=' + args.taskID
cmdline.execute(command.split())
