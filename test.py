import argparse
parser = argparse.ArgumentParser(description='这是一个示例程序，演示如何使用argparse读取命令行参数。')

# 添加命令行参数
parser.add_argument('--taskid', type=int, required=True)
parser.add_argument('--url', type=str, required=True)

# 解析命令行参数
args = parser.parse_args()

print(args.taskid)