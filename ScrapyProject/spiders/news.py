import json
import re
import time
from urllib.parse import urljoin, urlparse

import scrapy
from lxml import etree
from ScrapyProject.items import TreeItem, NodeItem


class NewsSpider(scrapy.Spider):
    name = "news"
    urls = {}  # 记录包含的所有链接
    stop = False
    depth = 0

    def __init__(self, url="https://default.com", domain="default.com", taskID=1, *args, **kwargs):
        super(NewsSpider, self).__init__(*args, **kwargs)
        if url:
            self.start_urls = [url]
        if domain:
            self.allowed_domains = [domain]
        self.urls[url] = {
            'father': 'root',
            'ifdone': True,
            'status': 0
        }
        self.statistic = {
            'image': 0,
            'video': 0,
            'audio': 0,
            'text': 0,
        }
        self.taskID = taskID
        self.begin_time = time.strftime("%Y-%m-%d-%H-%M", time.localtime())

    # 处理内容格式
    def file_format(self, link):
        image_formats = r"\.(jpeg|jpg|png|gif|bmp|tiff|tif|webp|svg|heic|heif|raw|cr2|nef|arw)$"
        video_formats = r"\.(mp4|avi|mkv|mov|wmv|flv|webm|mpeg|mpg|m4v)$"
        audio_formats = r"\.(mp3|wav|flac|aac|ogg|wma|m4a|alac)$"

        if isinstance(link, list):
            link = "".join(link)
        if re.search(image_formats, link, re.IGNORECASE) is not None:
            return 'image'
        elif re.search(video_formats, link, re.IGNORECASE) is not None:
            return 'video'
        elif re.search(audio_formats, link, re.IGNORECASE) is not None:
            return 'audio'
        else:
            return 'text'

    # 获取链接绝对路径
    def url_process(self, web_url, relative_url):
        former_url = web_url
        latter_url = relative_url

        # if 'http' in relative_url:
        #     return relative_url
        # if relative_url in web_url:
        #     return web_url
        # if '__local' in relative_url:
        #     if 'http' in web_url:
        #         former_url = former_url[:former_url.find('/', 8)]
        #         if relative_url.startswith('/'):
        #             return former_url + relative_url
        #         else:
        #             return former_url + '/' + relative_url
        #
        # while latter_url.startswith('./'):
        #     latter_url = latter_url[2:]
        # while latter_url.startswith('../'):
        #     latter_url = latter_url[3:]
        #     former_url = former_url[:former_url.rfind('/')]
        #     while former_url[-1] is not '/':
        #         former_url = former_url[:-1]
        # if former_url[-1] == '/' and latter_url[0] == '/':
        #     former_url = former_url[:-1]
        # if former_url[-1] != '/' and latter_url[0] != '/':
        #     former_url = former_url + '/'
        return urljoin(former_url, latter_url)

    # 检查链接是否为文本内容
    def link_format(self, link):
        pattern = r'\.(jpg|jpeg|png|gif|bmp|mp4|mkv|avi|mov|webm|flv|svg)$'
        return bool(re.search(pattern, link, re.IGNORECASE))

    # 检查链接是否在域名内
    def link_domain(self, link):
        parsed_url = urlparse(link)
        for domain in self.allowed_domains:
            if not parsed_url.netloc.endswith(domain):
                return 1
        if self.link_format(link):
            return 200
        return 0

    def add_child(self, children, url, value):
        if children:

            for child in children:
                if child["url"] == value["father"]:
                    child["children"].append({
                        "name": "子节点",
                        "url": url,
                        "status": value["status"],
                        "children": []
                    })
                    return
                c = child["children"]
                self.add_child(c, "url", value)

    # 转换为树结构

    def raw2tree(self):
        data = {
            "name": "根节点",
            "url": "",
            "children": []
        }
        urls = self.urls
        for url, value in urls.items():
            if value["father"] == "root":
                data["url"] = url
            else:
                if value["father"] == data["url"]:
                    data["children"].append({
                        "name": "子节点",
                        "url": url,
                        "status": value["status"],
                        "children": []
                    })
                else:
                    c = data["children"]
                    self.add_child(c, url, value)

        return data

    # 网站内遍历
    def traverse_child(self, response, xpath, url_now):
        # print('-----print xpath-----')
        # print(xpath)

        # 获取当前节点文本内容
        content = response.xpath('./text()').extract()
        if content:
            if type(content) is list:
                content = "".join(content)
            node = NodeItem()
            #node['father_url'] = response.request.headers.get('Referer', b'No Referer').decode('utf-8')
            node['taskID'] = self.taskID
            node['begin_time'] = self.begin_time
            node['web_url'] = url_now
            node['xpath'] = xpath
            node['content'] = content
            node['node_type'] = 'content'
            node['content_type'] = self.file_format(content)
            node['collect_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            self.statistic[node['content_type']] += 1
            if node['content_type'] != 'text':
                node['content'] = self.url_process(url_now, content)
            yield node
            # print('-----print content-----')
            # print(content)

        # 获取当前节点属性内容
        attributes = response.attrib
        if attributes:

            # 记录属性类型名和值
            # print('-----print attributes-----')
            for name, value in attributes.items():
                if type(value) is list:
                    value = "".join(value)
                node = NodeItem()
                # node['father_url'] = response.request.headers.get('Referer', b'No Referer').decode('utf-8')
                node['taskID'] = self.taskID
                node['begin_time'] = self.begin_time
                node['web_url'] = url_now
                node['xpath'] = xpath
                node['content'] = value
                node['node_type'] = name
                node['content_type'] = self.file_format(value)
                node['collect_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

                self.statistic[node['content_type']] += 1
                # 图片/视频..链接地址转换
                if node['content_type'] != 'text':
                    node['content'] = self.url_process(url_now, value)
                # 记录新的跳转链接
                if name == 'href' and not value.startswith('java'):
                    new_url = self.url_process(url_now, value)
                    node['content'] = new_url
                    if new_url not in self.urls:
                        self.urls[new_url] = {
                            'father': url_now,
                            'ifdone': self.link_format(new_url),  # 非文本内容标记为true不再遍历
                            'status': self.link_domain(new_url)  # 链接不在domain内标记为1，非文本内容记200
                        }

                yield node

        # 获取当前节点的所有子节点
        tag_count = {}
        children = response.xpath('./*')
        for i, child in enumerate(children, start=1):

            # xpath路径更新
            tag_name = child.root.tag
            if tag_name in tag_count:
                tag_count[tag_name] += 1
            else:
                tag_count[tag_name] = 1
            child_xpath = f"{xpath}/{tag_name}[{tag_count[tag_name]}]"
            # 迭代遍历全部节点
            yield from self.traverse_child(child, child_xpath, url_now)

    def parse_single(self, url):
        pass

    def parse(self, response, *args, **kwargs):
        # 记录链接状态码
        self.urls[response.url]['status'] = response.status
        #print(response.status)
        # 每个链接从html根结点开始
        xpath = '//html'  # + '/body/div[3]/div[1]/div[3]' #+ '/ul[1]/li[1]/span[1]'
        children = response.xpath(xpath)
        yield from self.traverse_child(children, xpath, response.url)
        self.depth += 1
        limit = 1e10
        if True and self.depth < 10:
            # 遍历当前网站下所有的跳转链接
            for url, value in self.urls.items():
                if not value['ifdone']:
                    self.urls[url]['ifdone'] = True
                    yield scrapy.Request(url=url, callback=self.parse)

        tree = TreeItem()
        tree['taskID'] = self.taskID
        tree['begin_time'] = self.begin_time
        tree['json_format'] = self.urls
        tree['tree_format'] = self.raw2tree()
        tree['collect_time'] = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        tree['datas'] = self.statistic
        yield tree
