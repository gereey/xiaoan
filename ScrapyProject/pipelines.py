# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import json
import pymysql
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from scrapy import Item

from ScrapyProject.items import NodeItem, TreeItem


class SqlPipeline:
    def __init__(self):
        self.cursor = None
        self.connection = None
        self.item = None
        self.count = 0

    def open_spider(self, spider):
        self.connection = pymysql.connect(
            host="10.61.0.51",
            user="root",
            passwd="root",
            charset="utf8",
            use_unicode=False
        )
        self.cursor = self.connection.cursor()

    def close_spider(self, spider):
        self.save_item()
        self.cursor.close()
        self.connection.close()

    def process_item(self, item, spider):
        if isinstance(item, TreeItem):
            self.count += 1
            self.item = item
            if self.count >= 500:
                self.save_item()
                self.count = 0
        return item

    def save_item(self):
        item = self.item
        cursor = self.cursor
        cursor.execute("USE xiaoan")
        # 插入数据库
        sql = "INSERT INTO record(taskId,tree,executeTime,data) VALUES(%s,%s,%s,%s)"
        try:
            cursor.execute(sql,
                           (item['taskID'],
                            json.dumps(item['tree_format']),
                            item['collect_time'],
                            json.dumps(item['datas'])))
            cursor.connection.commit()
        except BaseException as e:
            print("错误在这里>>>>>>>>>>>>>", e, "<<<<<<<<<<<<<错误在这里")
            self.connection.rollback()


class ScrapyprojectPipeline:
    def process_item(self, item, spider):
        print('----------begin print----------\n')
        return item


class PrintPipeline:
    def process_item(self, item, spider):
        print(item)
        return item


class WritePipeline:
    def __init__(self):
        self.file1 = None
        self.file2 = None
        self.file3 = None

    def open_spider(self, spider):
        # 在爬虫开始时打开文件
        self.file1 = open('news.json', 'w', encoding="utf8")
        self.file2 = open('zf.json', 'w', encoding="utf8")
        self.file3 = open('zf_url.json', 'w', encoding="utf8")

    def close_spider(self, spider):
        # 在爬虫关闭时关闭文件
        self.file1.close()
        self.file2.close()
        self.file3.close()

    def process_item(self, item, spider):
        # 将 item 转换为 JSON 并写入文件
        if isinstance(item, NodeItem):
            line = json.dumps(dict(item), ensure_ascii=False) + "\n"
            self.file2.write(line)
        if isinstance(item, TreeItem):
            line = json.dumps(dict(item), ensure_ascii=False) + "\n"
            self.file3.write(line)

        return item


class CustomWritePipeline:
    def __init__(self):
        self.files = None
        self.tree = None
        self.tag = 0

    def open_spider(self, spider):
        self.files = {}

    def close_spider(self, spider):
        self.save_tree()
        # 关闭所有打开的文件
        for file in self.files.values():
            file.close()

    def process_item(self, item, spider):
        task_id = item.get('taskID', 'DEFAULT')
        time = item.get('begin_time', 'DEFAULT')
        if isinstance(item, NodeItem):
            file_name = f"./ScrapyProject/data/{task_id}_{time}_node.json"
            if file_name not in self.files:
                self.files[file_name] = open(file_name, 'w', encoding="utf8")

            line = json.dumps(dict(item), ensure_ascii=False) + "\n"
            self.files[file_name].write(line)

        if isinstance(item, TreeItem):
            self.tree = item
            self.tag += 1
            if self.tag >= 1000:
                self.save_tree()
                self.tag = 0

        return item

    def save_tree(self):
        item = self.tree
        task_id = item.get('taskID', 'DEFAULT')
        time = item.get('begin_time', 'DEFAULT')

        file_name = f"../ScrapyProject/data/{task_id}_{time}_tree.json"
        if file_name not in self.files:
            self.files[file_name] = open(file_name, 'w', encoding="utf8")

        line = json.dumps(dict(item), ensure_ascii=False) + "\n"
        self.files[file_name].write(line)
        self.tag += 1
