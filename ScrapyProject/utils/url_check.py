import json
from tqdm import tqdm
import threading

semaphore = threading.Semaphore(20)
cnt = 0
str_cnt = 0

def url_check(url, contents):
    with semaphore:
        if url in contents:
            global cnt
            cnt += 1
            return
        print(url)


with open('../../data/zf_url_single.json', 'r', encoding='utf-8') as f:
    with open('../../data/zf.json', 'r', encoding='utf-8') as f1:
        contents = f1.readlines()
        urls = json.load(f)
        print(len(contents))
        print(len(urls))
        url_list = []
        for content in tqdm(contents):
            for i in json.loads(content)['content']:
                str_cnt += len(str(i))
            if json.loads(content)['web_url'] not in url_list:
                url_list.append(json.loads(content)['web_url'])
        print(len(url_list))

        threads = []
        for url, info in tqdm(urls.items()):
            t = threading.Thread(target=url_check, args=(url, url_list))
            threads.append(t)

        for t in tqdm(threads):
            t.start()

    print(cnt)
    print(str_cnt)
