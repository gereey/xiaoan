import json
import re

data = {
    "name": "根节点",
    "url": "",
    "children": []
}

def add_child(children, url, value):
    if children:

        for child in children:
            if child["url"] == value["father"]:
                child["children"].append({
                    "name": "子节点",
                    "url": url,
                    "children": []
                })
                return
            c = child["children"]
            add_child(c,"url", value)


with open("../../news_url.json", "r") as f:
    urls = json.load(f)
    for url, value in urls.items():
        if value["father"] == "root":
            data["url"] = url
        else:
            if value["father"] == data["url"]:
                data["children"].append({
                    "name": "子节点",
                    "url": url,
                    "children": []
                })
            else:
                c = data["children"]
                add_child(c, url, value)

f = open("../../tree.json", "w", encoding="utf8")
f.write(json.dumps(dict(data), ensure_ascii=False))
f.close()

