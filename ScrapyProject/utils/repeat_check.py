import json
import sys


# 拿到上次爬取的所有结点
def get_last_node(file, flag):
    with open(file, 'r', encoding='utf-8') as f:
        lines = f.readlines()
        target = []
        for line in lines:
            json_format = json.loads(line)
            # if json_format.get('web_url', "none") == flag:
            target.append(json_format)
    f.close()
    return target


def get_father_xpath(item):
    xpath = item.get('xpath', '//html')[2:]
    if '/' not in xpath:
        return xpath
    xpath = "/".join(xpath.split('/')[:-1])
    return xpath


def check_father_xpath(new, old):
    same_xpath = []
    xpath_new = get_father_xpath(new)
    for i in old:
        xpath_old = get_father_xpath(i)
        if xpath_old == xpath_new:
            same_xpath.append(i)
    return same_xpath


def repeat_check(new, old):
    old = check_father_xpath(new, old)  # get the same father xpath as the 'new' one
    for i in old:
        c_new = "".join(new.get('content', "none"))
        c_old = "".join(i.get('content', "none"))
        if c_new == c_old:
            return False
    return True


if __name__ == "__main__":
    new_file = sys.argv[1]
    old_file = sys.argv[2]
    new_item = []
    with open(new_file, 'r', encoding='utf-8') as f1:
        lines = f1.readlines()

        url = "http://zfai.buaa.edu.cn/"
        former = get_last_node(old_file, url)
        for line in lines:
            json_format = json.loads(line)
            if repeat_check(json_format, former):
                new_item.append(json_format)
    f1.close()
    for i in new_item:
        print(i)
    with open(new_file, 'w', encoding='utf-8') as f1:
        for i in new_item:
            j = json.dumps(i, ensure_ascii=False) + "\n"
            f1.write(j)
    f1.close()

# xpath = "//html/body[1]/div[2]/div[1]/div[1]/div[1]/a[1]/img[1]"
# print(xpath[2:])
# xpath = xpath[2:]
# xpath = "/".join(xpath.split('/')[:-1])
# print(xpath)
