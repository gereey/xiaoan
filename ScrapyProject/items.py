# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class NodeItem(scrapy.Item):
    taskID = scrapy.Field()
    begin_time = scrapy.Field()  # 当前任务的启动时间
    father_url = scrapy.Field()  # 暂时无法使用
    web_url = scrapy.Field()  # 节点当前网站的url
    xpath = scrapy.Field()  # 节点当前网站的xpath
    node_type = scrapy.Field()  # 节点类型：节点文本/节点属性
    content_type = scrapy.Field()  # 内容类型：文字/图片...
    content = scrapy.Field()  # 具体内容
    collect_time = scrapy.Field()  # 获取时间


class TreeItem(scrapy.Item):
    taskID = scrapy.Field()  # 任务ID
    begin_time = scrapy.Field()  # 当前任务的启动时间
    json_format = scrapy.Field()  # 原始url表
    tree_format = scrapy.Field()  # 处理后的树结构
    collect_time = scrapy.Field()
    datas = scrapy.Field()  # 统计数据
