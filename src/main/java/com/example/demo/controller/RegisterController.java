package com.example.demo.controller;

import com.example.demo.entity.Record;
import com.example.demo.entity.RegisterWebsite;
import com.example.demo.entity.checkResult;
import com.example.demo.service.BlackWebsiteService;
import com.example.demo.service.CallXiaoanAPIService;
import com.example.demo.service.DynamicScheduledTaskService;
import com.example.demo.service.RegisterWebsiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

@RestController
public class RegisterController {

    @Autowired
    private DynamicScheduledTaskService dynamicScheduledTaskService;
    @Autowired
    private CallXiaoanAPIService callXiaoanAPIService;

    @Autowired
    public RegisterWebsiteService registerWebsiteService;
    @PostMapping("/register/get")
    public List<RegisterWebsite> getList(@RequestBody Map<String,Object> params) {
        return registerWebsiteService.getList(params);
    }

    @GetMapping("/record/get")
    public Record getRecordById(@RequestParam(value = "id") Integer id ) {
        return registerWebsiteService.getRecordById(id);
    }


    @PostMapping("/register/add")
    public String add(@RequestBody Map<String,Object> params) {
        String website = (String) params.get("website");
        String title =  (String) params.get("title");
        Integer checkLevel =  (Integer) params.get("checkLevel");
        String lastScanTime = (String) params.get("lastScanTime");
        String warningStatus = (String) params.get("warningStatus");
        RegisterWebsite registerWebsite = new RegisterWebsite();
        registerWebsite.setWarningStatus(warningStatus);
        registerWebsite.setLastScanTime(lastScanTime);
        registerWebsite.setTitle(title);
        registerWebsite.setWebsite(website);
        registerWebsite.setCheckLevel(checkLevel);
        registerWebsiteService.add(registerWebsite);
        new Thread(() -> {
            dynamicScheduledTaskService.add(params,registerWebsite.getId());
            callXiaoanAPIService.add(registerWebsite.getId());
        }).start();

        return "success";
    }
    @PostMapping("/register/delete")
    public String delete(@RequestBody Map<String,Object> params) {
        String title = params.get("title").toString();
        dynamicScheduledTaskService.cancel(title);
        registerWebsiteService.delete(params);
        return "success";
    }

    @GetMapping("/register/get")
    public RegisterWebsite getById(@RequestParam(value = "id") Integer id ) {
        return registerWebsiteService.getById(id);
    }

    @GetMapping("/result/get")
    public List<checkResult> getCheckResultById(@RequestParam(value = "id") Integer id ) {
        return registerWebsiteService.getCheckResultById(id);
    }
}
