package com.example.demo.controller;

import com.example.demo.entity.BlackWebsite;
import com.example.demo.service.BlackWebsiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class BlackController {
    @Autowired
    public BlackWebsiteService blackWebsiteService;

    @PostMapping("/black/add")
    public String addBlackWebsite(@RequestBody Map<String,Object> params) {
        System.out.println(params);
        int status = blackWebsiteService.addBlackWebsite(params);
        if (status == 1) { return "success";}
        return "fail";
    }

    @GetMapping("/black/get")
    public List<BlackWebsite> getBlackWebsite() {
        return blackWebsiteService.getAllBlackWebsite();
    }

    @PostMapping("/black/delete")
    public String deleteBlackWebsite(@RequestBody Map<String,Object> params) {
        int status = blackWebsiteService.deleteBlackWebsite(params);
        if (status == 1) { return "success";}
        return "fail";
    }

    

}
