package com.example.demo.mapper;

import com.example.demo.entity.BlackWebsite;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BlackWebsiteMapper {
    @Select("select * from black_website")
    public List<BlackWebsite> getList();

    @Select("select * from black_website where id = #{id}")
    public BlackWebsite getById(long id);

    @Insert("INSERT INTO black_website (id,web_ip,legal_port,port_to_service,update_time) VALUES (null,#{web_ip}, #{legal_port},NULL,#{update_time})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public int insert(BlackWebsite blackWebsite);


    public int update(BlackWebsite blackWebsite);

    @Delete("DELETE FROM black_website WHERE id = #{id};")
    public int delete(Integer id);

}
