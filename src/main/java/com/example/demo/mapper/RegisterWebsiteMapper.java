package com.example.demo.mapper;

import com.example.demo.entity.Record;
import com.example.demo.entity.RegisterWebsite;
import com.example.demo.entity.checkResult;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface RegisterWebsiteMapper {
    @Select("select * from register_website")
    public List<RegisterWebsite> getList();

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("INSERT INTO register_website(id, website,title,checkLevel,lastScanTime,warningStatus) VALUES (NULL,#{website},#{title},#{checkLevel},#{lastScanTime},#{warningStatus})")
    public int add(RegisterWebsite registerWebsite);

    @Delete("DELETE FROM register_website WHERE id = #{id};")
    public int delete(Integer id);

    @Select("select * FROM register_website WHERE id = #{id};")
    public RegisterWebsite getById(Integer id);

    @Select("select * FROM record WHERE taskId = #{taskId} ORDER BY id DESC LIMIT 1;")
    public Record getRecordById(Integer taskId);

    @Select("select * FROM web_content WHERE register_web_id = #{id} and risk_level = 'high'")
    public List<checkResult> getCheckResultById(Integer id);
}