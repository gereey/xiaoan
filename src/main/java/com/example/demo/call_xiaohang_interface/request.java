package com.example.demo.call_xiaohang_interface;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



public class request {
    public static List<Task> sendRequest(String filePath) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String dburl = "jdbc:mysql://10.61.0.51:3306/xiaoan";
        String username = "root";
        String password = "root";


        List<Task> tasks = new ArrayList<>();

        ExecutorService executorService = Executors.newFixedThreadPool(5);
        // 按行读取文件内容
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine())!= null) {
                String finalLine = line;
                executorService.execute(() -> {
                    // 线程执行的代码
                    ObjectMapper objectMapper = new ObjectMapper();
                    String data = "";
                    Task task = new Task();
                    try {
                        // 将字符串转换为 Java 对象（一个Task类）
                        task = objectMapper.readValue(finalLine, Task.class);
                        // 打印 Java 对象中的属性值
                        // System.out.println(task.getContent());
                        data = "{\"scene\": \"chat\", \"text\": \"" + task.getContent() + "\"}";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //发送请求调用小安接口
                    OkHttpClient client = new OkHttpClient();

                    // 创建一个 POST 请求
                    RequestBody body = RequestBody.create(MediaType.get("application/json"), data );
                    Request request = new Request.Builder()
                            .url("https://api.xhang.buaa.edu.cn:28119/apps/green/text")
                            .header("Content-Type", "application/json")
                            .header("x-api-key", "89264da2-c0bf-55d0-9c8e-9c85d5c372be")
                            .post(body)
                            .build();

                    try {
                        // 发送请求并获取响应
                        okhttp3.Response response = client.newCall(request).execute();
                        if (response.isSuccessful()) {
                            //处理响应结果
                            String res = response.body().string();
                            String res1 = res.split(",\"RiskLevel\":\"")[0];
                            String res2 = res1.substring(10, res1.length());
                            String res3 = res.split(",\"RiskLevel\":\"")[1];
                            String res4 = res3.substring(0, res3.length()-2);

                            task.setResult(res2);
                            task.setRisk_level(res4);

                            tasks.add(task);
                            //System.out.println(task.toString());
                            //处理数据库

                            try (Connection connection = DriverManager.getConnection(dburl, username, password);

                                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO  web_content (url,content,sensitive_words,risk_level) VALUES (?,?,?,?)")) {
                                preparedStatement.setString(1, task.getWeb_url());
                                preparedStatement.setString(2, task.getContent());
                                preparedStatement.setString(3, task.getResult());
                                preparedStatement.setString(4, task.getRisk_level());
                                int rowsAffected = preparedStatement.executeUpdate();
                                //System.out.println("Rows affected: " + rowsAffected);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                 //System.out.println(Thread.currentThread().getName() + " is running.");
                });
            }
            executorService.shutdown();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return tasks;
    }

}
