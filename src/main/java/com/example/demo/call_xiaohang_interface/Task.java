package com.example.demo.call_xiaohang_interface;


public class Task {
    private String taskID;
    private String begin_time;
    private String web_url;
    private String xpath;
    private String content;
    private String node_type;
    private String content_type;
    private String collect_time;
    private String result;
    private String risk_level;

    public String getTaskID() {
        return taskID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public String getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(String begin_time) {
        this.begin_time = begin_time;
    }

    public String getWeb_url() {
        return web_url;
    }

    public void setWeb_url(String web_url) {
        this.web_url = web_url;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNode_type() {
        return node_type;
    }

    public void setNode_type(String node_type) {
        this.node_type = node_type;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getCollect_time() {
        return collect_time;
    }

    public void setCollect_time(String collect_time) {
        this.collect_time = collect_time;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRisk_level() {
        return risk_level;
    }

    public void setRisk_level(String risk_level) {
        this.risk_level = risk_level;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskID='" + taskID + '\'' +
                ", begin_time='" + begin_time + '\'' +
                ", web_url='" + web_url + '\'' +
                ", xpath='" + xpath + '\'' +
                ", content='" + content + '\'' +
                ", node_type='" + node_type + '\'' +
                ", content_type='" + content_type + '\'' +
                ", collect_time='" + collect_time + '\'' +
                ", result='" + result + '\'' +
                ", risk_level='" + risk_level + '\'' +
                '}';
    }
}
