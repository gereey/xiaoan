package com.example.demo.entity;

import lombok.Data;

@Data
public class checkResult {
    public int id;
    private String url;
    private String content;
    private String sensitive_words;
    private String risk_level;
    private String register_web_id;
}
