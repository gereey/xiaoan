package com.example.demo.entity;

import lombok.Data;

@Data
public class Record {
    private int id;
    private int taskId;
    private String tree;
    private String data;
    private String executeTime;
}
