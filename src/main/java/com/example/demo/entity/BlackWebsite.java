package com.example.demo.entity;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BlackWebsite {
    private Long id;

    private String web_ip;

    private String legal_port;

    private String port_to_service;

    private String update_time;
}
