package com.example.demo.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RegisterWebsite {
    private Long id;

    private String title;

    private String website;

    private Integer checkLevel;

    private String lastScanTime;

    private String warningStatus;
}
