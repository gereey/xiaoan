package com.example.demo.service;

import com.example.demo.entity.BlackWebsite;
import com.example.demo.entity.DynamicScheduledTaskRegistrar;
import com.example.demo.mapper.BlackWebsiteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class BlackWebsiteService {
    @Autowired
    public BlackWebsiteMapper blackWebsiteMapper;

    private static final Logger log = LoggerFactory.getLogger(DynamicScheduledTaskService.class);

    private final DynamicScheduledTaskRegistrar dynamicScheduledTaskRegistrar = new DynamicScheduledTaskRegistrar();

    public int addBlackWebsite(Map<String,Object> params) {
        BlackWebsite blackWebsite = new BlackWebsite();
        String ip = (String) params.get("ip");
        String legal_port = (String) params.get("legal_port");
        String update_time = (String) params.get("update_time");

        blackWebsite.setWeb_ip(ip);
        blackWebsite.setLegal_port(legal_port);
        blackWebsite.setUpdate_time(update_time);
        int status = blackWebsiteMapper.insert(blackWebsite);
        if (status == 1) {
            //如果插入成功，进行port_to_service处理
            new Thread(() -> {
                launchScheduledTask(blackWebsite);
            }).start();
        }
        return status;
    }
    public List<BlackWebsite> getAllBlackWebsite() {
        return blackWebsiteMapper.getList();
    }

    public int deleteBlackWebsite(Map<String,Object> params) {
        Integer id = (Integer) params.get("id");

        dynamicScheduledTaskRegistrar.cancelCronTask(id.toString());
        return blackWebsiteMapper.delete(id);
    }

    private String launchScheduledTask(BlackWebsite blackWebsite) {


        String ip = blackWebsite.getWeb_ip();
        Long id = blackWebsite.getId();
        String cron = "0 0 0-23 * * ?";
        creatTask(ip,id);
        dynamicScheduledTaskRegistrar.addCronTask(id.toString(),cron,()->creatTask(ip,id));
        Boolean result = dynamicScheduledTaskRegistrar.addCronTask(blackWebsite.getWeb_ip(),cron,()->creatTask(ip,id));
        log.info("定时任务添加结果：" + result);

        return "success";
    }
    private void creatTask(String ip,Long id){
        Logger log = LoggerFactory.getLogger(DynamicScheduledTaskService.class);
        List<String> command = new ArrayList<>();
        command.add("python");
        command.add("./test2.py");
        command.add("--id");
        command.add(String.valueOf(id));
        command.add("--ip");
        command.add(ip);
        try {
            // 使用 ProcessBuilder 创建进程
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            Process process = processBuilder.start();

            // 读取标准输出和错误输出
            readOutput(process.getInputStream(), "STDOUT");
            readOutput(process.getErrorStream(), "STDERR");

            // 进程结束
            int exitCode = process.waitFor();
//            System.out.println("标准输出：\n" + normalOutput.toString());
//            System.out.println("错误输出：\n" + errorOutput.toString());
            System.out.println("退出状态码：" + exitCode);

            // 根据需要处理输出和结果
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info(ip+"结束");
    }
    private static void readOutput(final InputStream inputStream, final String type) {
        new Thread(() -> {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    System.out.println(type + ": " + line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
