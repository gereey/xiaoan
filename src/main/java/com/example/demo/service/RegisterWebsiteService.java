package com.example.demo.service;

import com.example.demo.entity.Record;
import com.example.demo.entity.RegisterWebsite;
import com.example.demo.entity.checkResult;
import com.example.demo.mapper.RegisterWebsiteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RegisterWebsiteService {
    @Autowired
    public RegisterWebsiteMapper registerWebsiteMapper;

    public int add(RegisterWebsite registerWebsite){
        return registerWebsiteMapper.add(registerWebsite);
    }

    public List<RegisterWebsite> getList(Map<String,Object> params) {
        return registerWebsiteMapper.getList();
    }

    public int delete(Map<String,Object> params) {
        Integer id = (Integer) params.get("id");
        return registerWebsiteMapper.delete(id);
    }

    public RegisterWebsite getById(Integer id) {
        return registerWebsiteMapper.getById(id);
    }

    public Record getRecordById(Integer id) {
        return registerWebsiteMapper.getRecordById(id);
    }

    public List<checkResult> getCheckResultById(Integer id){
        return registerWebsiteMapper.getCheckResultById(id);
    }
}
