package com.example.demo.service;

import com.example.demo.entity.Task;
import com.example.demo.entity.DynamicScheduledTaskRegistrar;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class CallXiaoanAPIService {


    private static final Logger log = LoggerFactory.getLogger(DynamicScheduledTaskService.class);

    private final DynamicScheduledTaskRegistrar dynamicScheduledTaskRegistrar = new DynamicScheduledTaskRegistrar();

    public void add(Long id){

        String filePath = getFilePath(id);
        System.out.println(filePath);
        String cron = "0 5 0 * * *";
        String taskName = "CONTENT_DETECTING";
        sendRequest(filePath,id);
        Boolean result = dynamicScheduledTaskRegistrar.addCronTask(taskName,cron,() -> sendRequest(filePath,id));
        log.info("定时任务添加结果：" + result);
    }

    /**
     * 取消任务
     * @param
     */
    public void cancel(String taskName){
        dynamicScheduledTaskRegistrar.cancelCronTask(taskName);
    }

    public static  String getFilePath(Long id){

        String directoryPath = "./ScrapyProject/data/";

        String keyword = id.toString();
        File directory = new File(directoryPath);

        if (directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null && files.length > 0) {
                List<String> fileNameList = new ArrayList<>();
                for (File file : files) {

                        // 获取文件名并用下划线分割
                        String fileName = file.getName();
                        String[] nameParts = fileName.split("_");

                        // 如果第一个字段包含关键词，打印文件路径
                        if (nameParts.length > 0 && nameParts[0].equals(keyword) && fileName.indexOf("node") > 0) {
                            fileNameList.add(file.getName());
                        }
                    }
                fileNameList.sort(Comparator.reverseOrder());
                return directoryPath + fileNameList.get(0);
                }
            }
        return null;
    }


    public static List<Task> sendRequest(String filePath,Long id) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String dburl = "jdbc:mysql://10.61.0.51:3306/xiaoan";
        String username = "root";
        String password = "root";


        List<Task> tasks = new ArrayList<>();

        ExecutorService executorService = Executors.newFixedThreadPool(5);
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine())!= null) {
                System.out.println(line);
                String finalLine = line;
                executorService.execute(() -> {
                    // 线程执行的代码
                    ObjectMapper objectMapper = new ObjectMapper();
                    String data = "";
                    Task task = new Task();
                    try {
                        // 将字符串转换为 Java 对象（一个Task类）
                        task = objectMapper.readValue(finalLine, Task.class);
                        // 打印 Java 对象中的属性值
                        // System.out.println(task.getContent());
                        data = "{\"scene\": \"chat\", \"text\": \"" + task.getContent() + "\"}";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //发送请求调用小安接口
                    OkHttpClient client = new OkHttpClient();

                    // 创建一个 POST 请求
                    RequestBody body = RequestBody.create(MediaType.get("application/json"), data );
                    Request request = new Request.Builder()
                            .url("https://api.xhang.buaa.edu.cn:28119/apps/green/text")
                            .header("Content-Type", "application/json")
                            .header("x-api-key", "89264da2-c0bf-55d0-9c8e-9c85d5c372be")
                            .post(body)
                            .build();

                    try (okhttp3.Response response = client.newCall(request).execute()){
                        // 发送请求并获取响应
                        if (response.isSuccessful()) {
                            //处理响应结果
                            String res = response.body().string();
//                            System.out.println(res);
                            String res1 = res.split(",\"RiskLevel\":\"")[0];
                            String res2 = res1.substring(10, res1.length());
                            String res3 = res.split(",\"RiskLevel\":\"")[1];
                            String res4 = res3.substring(0, res3.length()-2);

                            task.setResult(res2);
                            task.setRisk_level(res4);

                            tasks.add(task);
                            //System.out.println(task.toString());
                            //处理数据库

                            try (Connection connection = DriverManager.getConnection(dburl, username, password);

                                 PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO  web_content (url,content,sensitive_words,risk_level,register_web_id) VALUES (?,?,?,?,?)")) {
                                preparedStatement.setString(1, task.getWeb_url());
                                preparedStatement.setString(2, task.getContent());
                                preparedStatement.setString(3, task.getResult());
                                preparedStatement.setString(4, task.getRisk_level());
                                preparedStatement.setString(5, String.valueOf(id));
                                int rowsAffected = preparedStatement.executeUpdate();
                                //System.out.println("success");
//                                System.out.println("Rows affected: " + rowsAffected);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    System.out.println(Thread.currentThread().getName() + " is running.");
                });
                //request.sendRequest(line);
            }
            executorService.shutdown();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return tasks;
    }

}
