package com.example.demo.service;

import com.example.demo.entity.DynamicScheduledTaskRegistrar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Service
public class DynamicScheduledTaskService {

    private static final Logger log = LoggerFactory.getLogger(DynamicScheduledTaskService.class);

    private final DynamicScheduledTaskRegistrar dynamicScheduledTaskRegistrar = new DynamicScheduledTaskRegistrar();

    public void add(Map<String,Object> params,Long id){
        String website = (String) params.get("website");
        Integer checkLevel = (Integer) params.get("checkLevel");
        String cron = " 0 0/3 * * * ?";
        createTask(website,id);
        Boolean result = dynamicScheduledTaskRegistrar.addCronTask(website,cron,() -> createTask(website,id));
        log.info("定时任务添加结果：" + result);
    }

    /**
     * 取消任务
     * @param taskName
     */
    public void cancel(String taskName){
        dynamicScheduledTaskRegistrar.cancelCronTask(taskName);
    }

    private void createTask(String website,Long id){
        log.info(website+"开始");
        List<String> command = new ArrayList<>();
        command.add("python");
        command.add("./run.py");
        command.add("--taskID");
        command.add(String.valueOf(id));
        command.add("--url");
        command.add(website);
        try {
            // 使用 ProcessBuilder 创建进程
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            Process process = processBuilder.start();

            // 读取标准输出和错误输出
            readOutput(process.getInputStream(), "STDOUT");
            readOutput(process.getErrorStream(), "STDERR");

            // 进程结束
            int exitCode = process.waitFor();
//            System.out.println("标准输出：\n" + normalOutput.toString());
//            System.out.println("错误输出：\n" + errorOutput.toString());
            System.out.println("退出状态码：" + exitCode);

            // 根据需要处理输出和结果
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info(website+"结束");
    }

    private static void readOutput(final InputStream inputStream, final String type) {
        new Thread(() -> {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                String line;
                while ((line = reader.readLine()) != null) {
//                    System.out.println(type + ": " + line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
