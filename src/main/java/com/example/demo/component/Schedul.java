package com.example.demo.component;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

//@Component
public class Schedul {
    // 每隔5秒执行一次
    @Scheduled(fixedRate = 5000)
    public void executeTask() {
        System.out.println("定时任务正在执行：" + System.currentTimeMillis());
    }
}
