package com.example.demo.component;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class PythonSchedul {

    @Scheduled(fixedRate = 10000)
    public void runPythonScript() {
        try {
            List<String> command = new ArrayList<>();
            command.add("python");
            command.add("C:/Users/admin/PycharmProjects/pythonProject/test.py");

            ProcessBuilder processBuilder = new ProcessBuilder(command);
            Process process = processBuilder.start();
            // 获取 Python 脚本的输出
            // 读取标准输出
            BufferedReader normalReader = new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"));
            StringBuilder normalOutput = new StringBuilder();
            String line;
            while ((line = normalReader.readLine()) != null) {
                normalOutput.append(line).append("\n");
            }

            // 读取错误输出
            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream(), "GBK"));
            StringBuilder errorOutput = new StringBuilder();
            while ((line = errorReader.readLine()) != null) {
                errorOutput.append(line).append("\n");
            }

            int exitCode = process.waitFor();

            System.out.println("标准输出：\n" + normalOutput.toString());
            System.out.println("错误输出：\n" + errorOutput.toString());
            System.out.println("退出状态码：" + exitCode);

            // 根据需要处理输出和结果
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
