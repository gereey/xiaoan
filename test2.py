import nmap
import pymysql
import argparse
import json
parser = argparse.ArgumentParser(description='扫描黑网站')

# 添加命令行参数
parser.add_argument('--id', type=int, required=True)
parser.add_argument('--ip', type=str, required=True)

# 解析命令行参数
args = parser.parse_args()

def find_open_ports(ip,id):

    dict = {}
    nm = nmap.PortScanner()
    nm.scan(ip, '1-10000','-T5')

    for host in nm.all_hosts():
        for proto in nm[host].all_protocols():
            lport = nm[host][proto].keys()
            for port in lport:
                dict[port] = nm[host][proto][port]['name']


    # 连接数据库
    db = pymysql.connect(host='10.61.0.51', user='root', password='root', port=3306,db='xiaoan')
    try:
        print(dict)
        sql = 'UPDATE black_website SET port_to_service = %s WHERE id = %s'
        # 创建数据库的游标
        cursor = db.cursor()
        # 执行sql语句
        cursor.execute(sql, (json.dumps(dict), str(id)))
        db.commit()
    except:
        print("Error")
        # 有错误则回滚
        db.rollback()
    # 关闭数据库连接
    cursor.close()
    db.close()

find_open_ports(args.ip,args.id)





